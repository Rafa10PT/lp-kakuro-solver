:- [codigo_comum].
%:- [puzzles_publicos].

/* Repr 1: Os espacos sao representados
como espaco(soma, variaveis). */

/* Repr 2: lista de listas de 2 elementos, dos quais
o primeiro e' uma lista de casas do puzzle e o segundo
permutacoes possi'veis para essas casas. */

/* N - inteiro.
 * Els - lista de inteiros.
 * Soma - inteiro.
 * Comb - combinacoes N a N dos elementos de Els
 * cuja soma e' Soma.
 */
%combinacoes_soma(N, Els, Soma, Combs)
combinacoes_soma(N, Els, Soma, Combs) :-
    findall(X,
        (combinacao(N, Els, X), sum_list(X, Soma)), Combs).

/* N - inteiro.
 * Els - lista de inteiros.
 * Soma - inteiro.
 * Perm - lista ordenada cujos elementos sao permutacoes
 * das combinacoes N a N dos elementos de Els cuja soma e'
 * soma.
 */
%permutacoes_soma(N, Els, Soma, Perms)
permutacoes_soma(N, Els, Soma, Perms) :-
    combinacoes_soma(N, Els, Soma, Combs),
    findall(P, (member(X, Combs), permutation(X, P)), Perms_desord),
    % Temos de ordenar as posicoes.
    sort(Perms_desord, Perms).

/* Fila - linha/coluna de um puzzle.
 * Esp - espaco de Fila [Repr 1].
 * Soma - inteiro.
 * H_V - um dos a'tomos h (para horizontal).
 * ou v (para vertical).
 */
%espaco_fila(Fila, Esp, H_V)
espaco_fila(Fila, espaco(NUM, EspLst), H_V) :-
    % Elemento de Fila que seja lista no indice X.
    nth0(X, Fila, P), is_list(P),
    
    % Escolher o numero 'a direita caso seja h ou 'a esquerda caso seja v.
    (H_V == h -> P = [_, NUM];
    H_V == v -> P = [NUM, _]),

    % Tentar encontrar indice doutra lista logo a seguir e
    % caso falhe usar como limite o tamanho.
    (nth0(Lim, Fila, Val), Lim > X, is_list(Val), ! -> true; length(Fila, Lim)),

    % Obter todos os elementos entre os dois indices.
    bagof(Y, Idx^(nth0(Idx, Fila, Y), Idx > X, Idx < Lim), EspLst),

    % Nao podemos unificar com a lista vazia.
    EspLst \= [].

/* H_V - um dos a'tomos h (para horizontal).
 * Fila - linha/coluna de um puzzle.
 * Espacos - lista de todos os espacos de fila [Repr 1]
 * da esquerda para a direita.
 */
%espacos_fila(H_V, Fila, Espacos)
espacos_fila(H_V, Fila, Espacos) :-
    % Se nao encontrar, Espacos e' lista vazia.
    bagof(X, (espaco_fila(Fila, X, H_V)), Espacos); Espacos = [].

/* Puzzle - um puzzle proprimente dito.
 * Espacos - lista de todos os espacos de fila [Repr 1]
 * da esquerda para a direita.
 */
%espacos_puzzle(Puzzle, Espacos)
espacos_puzzle(Puzzle, Espacos) :-
    mat_transposta(Puzzle, Trans),
    maplist(espacos_fila(h), Puzzle, EspacosH),
    maplist(espacos_fila(v), Trans, EspacosV),
    !,
    % Juntar tudo e alisar.
    append(EspacosH, EspacosV, PrevEspacos),
    flatten(PrevEspacos, Espacos).

/* Predicado auxiliar.
 * Mem - universal (podendo ser varia'vel tambe'm).
 * Lst - lista.
 * Verifica se Mem e' membro de Lst.
 */
%membro_na_lista(Mem, Lst)
membro_na_lista(Mem, [P | R]) :- (Mem == P, !); membro_na_lista(Mem, R).

/* Predicado auxiliar.
 * Lst0/Lst1 - listas.
 * Verifica se as listas partilham algum membro.
 */
%membro_comum(Lst0, Lst1)
membro_comum([P | R], Lst1) :- (membro_na_lista(P, Lst1), !);
membro_comum(R, Lst1).

/* Predicado auxiliar.
 * Espaco/Esp - espaco [Repr 1].
 * Verifica-se se ambos os espacos tem membro em comum
 * e nao coincidem.
 */
%espaco_com_posicao_comum(Esp, Espaco)
espaco_com_posicao_comum(Esp, Espaco) :-
    % Nao podemos coincidir.
    Espaco \== Esp,
    Esp = espaco(_, Lst0),
    Espaco = espaco(_, Lst1), !,
    membro_comum(Lst0, Lst1).

/* Espacos - lista de todos os espacos de fila [Repr 1]
 * da esquerda para a direita.
 * Esp - espaco [Repr 1].
 * Esps_com - lista de espacos [Repr 1] que partilham varia'veis
 * com Esp nao incluindo o mesmo.
 */
%espacos_com_posicoes_comuns(Espacos, Esp, Esps_com)
espacos_com_posicoes_comuns(Espacos, Esp, Esps_com) :-
    % Filtrar apenas os que tem posicao comum.
    include(espaco_com_posicao_comum(Esp), Espacos, Esps_com).

/* Espacos - lista de espacos [Repr 1].
 * Perms_soma - lista de listas de 2 elementos
 * o primeiro e' um espaco [Repr 1] e
 * o segundo uma lista ordenada de permutacoes cuja soma e'
 * a soma do espaco.
 */
%permutacoes_soma_espacos(Espacos, Perms_soma)
permutacoes_soma_espacos(Espacos, Perms_soma) :-
    bagof([Esp, Perms], Esp^Sum^Lst^Len^Els^Perms^(member(Esp, Espacos),
        Esp = espaco(Sum, Lst), length(Lst, Len),
        % Com os digitos possiveis,
        % encontrar todas as permutacoes soma para os espacos.
        findall(El, between(1, 9, El), Els),
        permutacoes_soma(Len, Els, Sum, Perms)), Perms_soma).

/* Predicado auxiliar.
 * Perm - permutacao possivel para a lista de inteiros do espaco Esp.
 * Perms_soma - lista de listas
 * tal como a de permutacoes_soma_espacos.
 * Esps_com - lista de espacos [Repr 1] que partilham varia'veis
 * com Esp nao incluindo o mesmo.
 * Esp_com - um espaco membro de Esps_com.
 */
%verifica_perm(Perm, Perms_soma, Esps_com, Esp_com)
verifica_perm(Perm, Perms_soma, Esps_com, Esp_com) :-
    % O indice do espaco comum e' o mesmo que o do valor na permutacao.
    nth0(Idx, Esps_com, Esp_com),
    nth0(Idx, Perm, Val),

    member([Esp_com, Perms], Perms_soma),
    % Na lista alisada estao todos os valores possiveis de existir no espaco,
    % mas repetem-se.
    flatten(Perms, Perms_flat),
    % Val tem de estar nessa lista para ser um valor va'lido.
    member(Val, Perms_flat), !.

/* Perm - permutacao possivel para a lista de inteiros do espaco Esp.
 * Esp - espaco [Repr 1].
 * Espacos - lista de espacos [Repr 1].
 * Perms_soma - lista de listas
 * tal como a de permutacoes_soma_espacos.
 */
%permutacao_possivel_espaco(Perm, Esp, Espacos, Perms_soma)
permutacao_possivel_espaco(Perm, Esp, Espacos, Perms_soma) :-
    % Obter espacos comuns.
    espacos_com_posicoes_comuns(Espacos, Esp, Esps_com), !,
    % Obter as permutacoes do espaco atual onde tem de estar Perm.
    member([Esp, Perms], Perms_soma),
    member(Perm, Perms),
    % A posicao intersetada pelo espaco comum tem de ser va'lida em ambos os
    % espacos para validar a permutacao. 
    maplist(verifica_perm(Perm, Perms_soma, Esps_com), Esps_com).

/* Espacos - lista de espacos [Repr 1].
 * Perms_soma - lista de listas
 * tal como a de permutacoes_soma_espacos.
 * Esp - espaco [Repr 1].
 * Perms_poss - lista de 2 elementos em que
 * o primeiro e' a lista de varia'veis de Esp e
 * o segundo a lista ordenada de permutacoes possi'veis para o
 * espaco Esp.
 */
%permutacoes_possiveis_espaco(Espacos, Perms_soma, Esp, Perms_poss)
permutacoes_possiveis_espaco(Espacos, Perms_soma, Esp, [Lst, Perms_poss_set]) :-
    findall(Perm_poss,
    permutacao_possivel_espaco(Perm_poss, Esp, Espacos, Perms_soma),
    Perms_poss_prev),
    % Reduzir repetidos a 1 instancia com list_to_set.
    list_to_set(Perms_poss_prev, Perms_poss_set),
    Esp = espaco(_, Lst).

/* Espacos - lista de espacos [Repr 1].
 * Perms_poss_esps - lista [Repr 2].
 */
%permutacoes_possiveis_espacos(Espacos, Perms_poss_esps)
permutacoes_possiveis_espacos(Espacos, Perms_poss_esps) :-
    permutacoes_soma_espacos(Espacos, Perms_soma),
    maplist(permutacoes_possiveis_espaco(Espacos, Perms_soma),
    Espacos, Perms_poss_esps).

/* Predicado auxiliar.
 * Verifica se todos os elementos sao iguais na lista Lst.
 */
%tudo_igual(Lst)
tudo_igual(Lst) :- chain(Lst, #=).

/* Lst_Perms - lista de permutacoes.
 * Numeros_comuns - lista de pares (pos, numero),
 * significando que todas as listas de Lst_perms tem
 * o numero "numero" na posicao "pos".
 */
%numeros_comuns(Lst_Perms, Numeros_comuns)
numeros_comuns(Lst_Perms, Numeros_comuns) :-
    mat_transposta(Lst_Perms, Trans),
    include(tudo_igual, Trans, Comuns), !,
    % Caso hajam grupos na transposta que se repitam, 
    % como fazemos findall depois,
    % reduzimo-los apenas a 1 instancia com list_to_set.
    list_to_set(Comuns, Comuns_set),
    findall((Idx, Val),
        (member(Lst, Comuns_set), Lst = [Val | _], nth1(Idx, Trans, Lst)),
        Numeros_comuns), !.

/* Predicado auxiliar.
 * Lst - Lista com as varia'veis/valores do espaco.
 * Par - par (indice, valor) para realizar
 * a atribuicao 'a varia'vel num indice e guardar
 * o valor comum.
 */
%atribui_guarda(Lst, Par)
atribui_guarda(Lst, (Idx, Val)) :-
    % Guardar na lista.
    nth1(Idx, Lst, Val).

/* Predicado auxiliar.
 * Perm_Possivel - Elemento da lista de atribui_comuns.
 */
%atribui_comuns_iter(Perm_Possivel)
atribui_comuns_iter(Perm_Possivel) :-
    Perm_Possivel = [Lst, Lst_Perms],
    numeros_comuns(Lst_Perms, Numeros_comuns),
    % Guardar os valores no indice respetivo para todos os comuns encontrados.
    maplist(atribui_guarda(Lst), Numeros_comuns).

/* Perms_Possiveis - lista de permutacoes possiveis como em Repr 2
 * mas com valores a substiruir varia'veis
 * se todos os espacos tiverem um valor em comum numa dada posicao.
 */
%atribui_comuns(Perms_Possiveis)
atribui_comuns(Perms_Possiveis) :-
    maplist(atribui_comuns_iter, Perms_Possiveis), !.

/* Predicado auxiliar.
 * Perm_Possivel - elemento de Perms_Possiveis de retira_impossiveis.
 * Nova_Perm_Possivel - resultado de retirar as impossi'veis como descrito em
 * retira_impossiveis.
 */
%retira_impossivel(Perm_Possivel, Nova_Perm_Possivel)
retira_impossivel([Lst, Perms], [Lst, Novas_Perms]) :-
    % Filtrar apenas as possiveis para Novas_Perms.
    findall(Perm, (Perm = Lst, member(Perm, Perms)), Novas_Perms), !.

/* Perms_Possiveis - lista de permutacoes possi'veis
 * como em atribui_comuns.
 * Novas_Perms_possiveis - lista que resulta de retirar as listas
 * de permutacoes impossi'veis de dentro de Perms_possiveis
 * (as que nao conseguem unificar com a lista das variaveis/valores).
 */
%retira_impossiveis(Perms_Possiveis, Novas_Perms_Possiveis)
retira_impossiveis(Perms_Possiveis, Novas_Perms_Possiveis) :-
    % Transformar cada grupo de permutacoes.
    maplist(retira_impossivel, Perms_Possiveis, Novas_Perms_Possiveis).

/* Predicado auxiliar.
 * Perms - lista de permutacoes atual na recursao.
 * Perms_anteriores - lista de permutacoes da recursao anterior.
 * Novas_Perms - lista simplificada (resultado das operacoes).
 */
%simplifica_aux(Perms, Perms_anteriores, Novas_Perms)
simplifica_aux(Perms, Perms_anteriores, Novas_Perms) :-
    % Caso Perms == Perms_anteriores, pararamos a recursao. 
    (Perms == Perms_anteriores, !, Perms = Novas_Perms);
    atribui_comuns(Perms),
    retira_impossiveis(Perms, Novas_Perms_Possiveis),
    simplifica_aux(Novas_Perms_Possiveis, Perms, Novas_Perms).

/* Perms_Possiveis - lista de permutacoes possi'veis
 * como em atribui_comuns.
 * Novas_Perms_Possiveis - resultado de simplificar Perms_possiveis
 * (aplicar atribui_comuns e retira_impossiveis ate' nao haver mais alteracoes).
 */
%simplifica(Perms_Possiveis, Novas_Perms_Possiveis)
simplifica(Perms_Possiveis, Novas_Perms_Possiveis) :-
    simplifica_aux(Perms_Possiveis, [], Novas_Perms_Possiveis).

/* Puzzle - puzzle propriamente dito.
 * Perms_possiveis - lista de permutacoes simplificada
 * (tal como se descreve em simplifica) para Puzzle.
 */
%inicializa(Puzzle, Perms_Possiveis)
inicializa(Puzzle, Perms_Possiveis) :-
    espacos_puzzle(Puzzle, Espacos),
    permutacoes_possiveis_espacos(Espacos, Perms_poss_esps),
    simplifica(Perms_poss_esps, Perms_Possiveis).

/* Predicado auxiliar.
 * Perm_possivel - elemento de Perms_Possiveis de escolhe_menos_alternativas.
 * Len - tamanho da lista de alternativas para as permutacoes.
 */
%tamanho_perm_possivel(Perm_possivel, Len)
tamanho_perm_possivel([_, Alternativas], Len) :-
    length(Alternativas, Len).

/* Perms_Possiveis - lista de permutacoes possi'veis
 * como em atribui_comuns.
 * Escolha - escolhe, dos elementos de Perms_possiveis, o primeiro
 * que conte'm menos permutacoes para o espaco e que tenha mais de uma
 * permutacao possi'vel.
 */
%escolhe_menos_alternativas(Perms_Possiveis, Escolha)
escolhe_menos_alternativas(Perms_Possiveis, Escolha) :-
    % Guardar tamanhos todos > 1.
    maplist(tamanho_perm_possivel, Perms_Possiveis, Tamanhos_prev),
    include(\=(1), Tamanhos_prev, Tamanhos),
    % Procurar o menor deles.
    min_list(Tamanhos, Min),
    
    % Devolver o primeiro grupo de permutacoes que tenha esse tamanho.
    member(Escolha, Perms_Possiveis),
    Escolha = [_, Alternativa],
    length(Alternativa, Len),
    Len =:= Min, !.

/* Escolha - um dos elementos de Perms_Possiveis escolhido tal como
 * no predicado anterior.
 * Perms_Possiveis - lista de permutacoes possi'veis
 * como em atribui_comuns.
 * Novas_Perms_Possiveis - resultado de exprimentar substituir o membro
 * Escolha em Perms_Possiveis por [Esp, [Perm]] tal que
 * Escolha = [Esp, Lst_Perms] e Perm e' membro de Lst_Perms.
 */
%experimenta_perm(Escolha, Perms_Possiveis, Novas_Perms_Possiveis)
experimenta_perm(Escolha, Perms_Possiveis, Novas_Perms_Possiveis) :-  
    Escolha = [Esp, Lst_Perms],
    member(Perm, Lst_Perms),
    Esp = Perm,
    % Substituir na lista pela escolha.
    select(Escolha, Perms_Possiveis, [Esp, [Perm]], Novas_Perms_Possiveis).

/* Perms_Possiveis - lista de permutacoes possi'veis
 * como em atribui_comuns.
 * Novas_Perms_Possiveis - resultado de aplicar escolhe_menos_alternativas
 * para obter uma escolha, exprimenta_perm com a Escolha e Perms_Possiveis
 * e simplifica ao resultado varias vezes enquanto o tamanho de
 * Perms_Possiveis e' superior a 1.
 */
%resolve_aux(Perms_Possiveis, Novas_Perms_Possiveis) :-
resolve_aux(Perms_Possiveis, Novas_Perms_Possiveis) :-
    % Vai repetir va'rias vezes enquanto Len > 1.
    % Situacoes impossiveis sao retrocedidas 'a u'ltima escolha.
    length(Perms_Possiveis, Len), Len > 1,
    escolhe_menos_alternativas(Perms_Possiveis, Escolha), !,
    experimenta_perm(Escolha, Perms_Possiveis, Novas_Perms_Possiveis_Por_Simpl),
    simplifica(Novas_Perms_Possiveis_Por_Simpl, Perms_recur),
    resolve_aux(Perms_recur, Novas_Perms_Possiveis), !.

/* Caso se atinja este co'digo, Novas_Perms_Possiveis ja'
corresponde a Perms_Possiveis. */
resolve_aux(Perms_Possiveis, Perms_Possiveis) :- !.

/* Puz - puzzle proprimente dito.
 * Este predicado vai resolver o puzzle passado como argumento.
 * Depois de resolvido, tera' as suas varia'veis todas substituidas por
 * nu'meros que respeitam as restricoes de Puz.
 */
%resolve(Puz)
resolve(Puz) :-
    inicializa(Puz, Perms_Possiveis),
    resolve_aux(Perms_Possiveis, _).
